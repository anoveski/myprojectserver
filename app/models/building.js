var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BuildingSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    rating: {
        type: Number,
        required: true
    },
    seller: {
        sellerId: {
            type: String,
            required: true
        },
        sellerName: {
            type: String,
            required: false
        },
        sellerSurname: {
            type: String,
            required: true
        },
        phoneNumber: {
            type: Number,
            required: true
        },
        seller_email: {
            type: String,
            required: true
        }
    },
    advertisedDate:{
        type: String,
        required: true
    },
    propertyUnit: {
        typeOfBuilding: {
            type: String,
            required: true
        },
        yearOfConstruction: {
            type: Number,
            required: true
        },
        area: {
            baseSize:{
                type: Number,
                required: true
            },
            yardStatus: {
                type: String,
                required: true
            },
            yardSize: {
                type: Number,
                required: true
            }
        },
        interior:{
            numberOfFloors:{
                type: Number,
                required: true
            },
            numberOfBathrooms:{
                type: Number,
                required: true
            },
            numberOfToilets:{
                type: Number,
                required: true
            },
            numberOfKitchens:{
                type: Number,
                required: true
            },
            numberOfRooms:{
                type: Number,
                required: true
            },
            numberCorridors:{
                type: Number,
                required: true
            }
        },
        location: {
            addressLine: {
                type: String,
                required: true
            },
            zipCode: {
                type: Number,
                required: true
            },
            place: {
                type: String,
                required: true
            },
            province: {
                type: String,
                required: true
            },
            country: {
                type: String,
                required: true
            },
            countryIsoCode:{
                type: String,
                required: true
            },
            gpsCoordinates: {
                latitude: {
                    type: Number,
                    required: true
                },
                longitude: {
                    type: Number,
                    required: true
                }
            }
        }
    },
    details: {
        type: String,
        required: true
    },
    buildingProfilePicture:{
        type: String,
        required: true
    },
    buildingGallery: {
        type: Array,
        required: true
    },
    statusOfTheParts: {
        bathroomsRating:{
            type: Number,
            required: true
        },
        toiletsRating:{
            type: Number,
            required: true
        },
        kitchenRating:{
            type: Number,
            required: true
        },
        roomsRating:{
            type: Number,
            required: true
        },
        corridorsRating:{
            type: Number,
            required: true
        },
        roofRating:{
            type: Number,
            required: true
        },
        windowsRating:{
            type: Number,
            required: true
        }
    }
});

module.exports = mongoose.model('Building', BuildingSchema);