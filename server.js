var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var mongo = require('mongodb');
var passport = require('passport');
var User = require('./app/models/user'); // get the mongoose model
var Building = require('./app/models/building');
var port = process.env.PORT || 8080;
var jwt = require('jwt-simple');
var multer = require('multer');
var logger = require('./config/logger');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

var dbConnectionString = process.env.DB_CONNECTION_STRING;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './BuildingsPhotos/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
});

function fileFilter(req, file, cb) {
    if (file.mimetype === 'image/jpg'
        || file.mimetype === 'image/jpeg'
        || file.mimetype === 'image/png'
        || file.mimetype === 'image/bmp') {
        cb(null, true)
    } else {
        cb(new Error("Picture type not supported! Please use PNG or JPG or BMP images type"), false)
    }
}

var upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

// get our request parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/BuildingsPhotos', express.static('BuildingsPhotos'));

app.use(function (req, res, next) {
    var allowedOrigins = process.env.ALLOWED_ORIGINS.split(',');
    var origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, PropertyType');
    next();
});

// log to console
app.use(morgan('dev'));

// Use the passport package in our application
app.use(passport.initialize());

// demo Route (GET http://localhost:8080)
app.get('/', function (req, res) {
    res.send('Hello! The API is at http://localhost:' + port + '/api');
});

mongoose.connect(dbConnectionString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
require('./config/passport')(passport);

var apiRoutes = express.Router();

apiRoutes.post('/register', function (req, res) {
    if (!req.body.first_name || !req.body.last_name ||
        !req.body.user_gender || !req.body.date_of_birth ||
        !req.body.phone_number || !req.body.user_email
        || !req.body.password) {
        res.json({success: false, message: 'Please enter valid credentials'});
    } else {
        var newUser = new User({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            user_gender: req.body.user_gender,
            date_of_birth: req.body.date_of_birth,
            phone_number: req.body.phone_number,
            user_email: req.body.user_email,
            password: req.body.password
        });
        newUser.save(function (err) {
            if (err) {
                res.json({success: false, message: "Not registered! Email already exists"})
            } else {
                res.json({success: true, message: 'Successful created user!'})
            }
        })
    }
});

// route to authenticate a user (POST http://localhost:8080/api/login)
apiRoutes.post('/login', function (req, res) {
    User.findOne({
        user_email: req.body.user_email
    }, function (err, user) {
        if (err) {
            logger.error("/api/login", err);
            throw err;
        }

        if (!user) {
            res.send({success: false, message: 'User not found.'});
        } else {
            // check if password matches
            user.comparePassword(req.body.password, function (err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    var token = jwt.encode(user, process.env.APPLICATION_SECRET);
                    // return the information including token as JSON
                    res.json({success: true, token: 'JWT ' + token});
                } else {
                    res.send({success: false, message: 'Wrong password.'});
                }
            });
        }
    });
});

// route to a restricted info (GET http://localhost:8080/api/user-info)
apiRoutes.get('/user-info', passport.authenticate('jwt', {session: false}), function (req, res) {
    var token = getToken(req.headers);
    if (token) {
        var decoded = jwt.decode(token, process.env.APPLICATION_SECRET);
        User.findOne({
            user_email: decoded.user_email
        }, function (err, user) {
            if (err) {
                logger.error("/api/user-info", err);
                throw err;
            }

            if (!user) {
                return res.status(403).send({success: false, message: 'Authentication failed. User not found.'});
            } else {
                res.json({
                    success: true,
                    user: {
                        id: user.id,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        user_gender: user.user_gender,
                        date_of_birth: user.date_of_birth,
                        phone_number: user.phone_number,
                        user_email: user.user_email
                    }
                });
            }
        });
    } else {
        return res.status(403).send({success: false, message: 'No token provided.'});
    }
});

getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

//get all properties
apiRoutes.get('/get-all-properties', function (req, res) {
    var propertiesArray = [];
    Building.find({}, function (err, properties) {
        if (err) {
            logger.error("/api/get-all-properties", err);
            throw err;
        } else {
            properties.forEach(function (property, err) {
                propertiesArray.push({
                    id: property._id,
                    title: property.title,
                    price: property.price,
                    rating: property.rating,
                    typeOfBuilding: property.propertyUnit.typeOfBuilding,
                    buildingProfilePicture: property.buildingProfilePicture,
                    province: property.propertyUnit.location.province,
                    place: property.propertyUnit.location.place,
                    country: property.propertyUnit.location.country
                });
            });
            res.json(propertiesArray);
        }
    })
});

//get property by ID
apiRoutes.get('/get-property-by/:id', function (req, res) {
    Building.findOne({
        _id: req.params.id
    }, function (err, property) {
        if (err) {
            logger.error("/api/get-property-by/ID", err);
            throw err;
        } else {
            res.json(property);
        }

    })
});

//get properties by type
apiRoutes.get('/get-properties-by-type', function (req, res) {
    var neededPropertyType = (req.headers.propertytype);
    var propertiesByTypeArray = [];
    Building.find({'propertyUnit.typeOfBuilding': neededPropertyType}, function (err, propertiesByType) {
        if (err) {
            logger.error("/api/get-properties-by-type", err);
            throw err;
        } else {
            propertiesByType.forEach(function (property, err) {
                propertiesByTypeArray.push({
                    id: property._id,
                    title: property.title,
                    price: property.price,
                    rating: property.rating,
                    typeOfBuilding: property.propertyUnit.typeOfBuilding,
                    buildingProfilePicture: property.buildingProfilePicture,
                    province: property.propertyUnit.location.province,
                    place: property.propertyUnit.location.place,
                    country: property.propertyUnit.location.country
                });
            });
            res.json(propertiesByTypeArray);
        }

    })
});

var cpUpload = upload.fields([{name: 'buildingProfilePicture', maxCount: 1}, {name: 'buildingGallery', maxCount: 12}]);
apiRoutes.post('/add-building', cpUpload, function (req, res, next) {
    var newBuilding = new Building({
        title: req.body.title,
        price: req.body.price,
        rating: req.body.rating,
        seller: {
            sellerId: req.body.sellerId,
            sellerName: req.body.sellerName,
            sellerSurname: req.body.sellerSurname,
            phoneNumber: req.body.phoneNumber,
            seller_email: req.body.seller_email
        },
        advertisedDate: req.body.advertisedDate,
        propertyUnit: {
            typeOfBuilding: req.body.typeOfBuilding,
            yearOfConstruction: req.body.yearOfConstruction,
            area: {
                baseSize: req.body.baseSize,
                yardStatus: req.body.yardStatus,
                yardSize: req.body.yardSize
            },
            interior: {
                numberOfFloors: req.body.numberOfFloors,
                numberOfBathrooms: req.body.numberOfBathrooms,
                numberOfToilets: req.body.numberOfToilets,
                numberOfKitchens: req.body.numberOfKitchens,
                numberOfRooms: req.body.numberOfRooms,
                numberCorridors: req.body.numberCorridors
            },
            location: {
                addressLine: req.body.addressLine,
                zipCode: req.body.zipCode,
                place: req.body.place,
                province: req.body.province,
                country: req.body.country,
                countryIsoCode: req.body.countryIsoCode,
                gpsCoordinates: {
                    latitude: req.body.latitude,
                    longitude: req.body.longitude
                }
            }
        },
        details: req.body.details,
        buildingProfilePicture: req.files['buildingProfilePicture'][0].path,
        buildingGallery: req.files['buildingGallery'],
        statusOfTheParts: {
            bathroomsRating: req.body.bathroomsRating,
            toiletsRating: req.body.toiletsRating,
            kitchenRating: req.body.kitchenRating,
            roomsRating: req.body.roomsRating,
            corridorsRating: req.body.corridorsRating,
            roofRating: req.body.roofRating,
            windowsRating: req.body.windowsRating
        }
    });
    newBuilding.save(function (err) {
        if (err) {
            logger.error("/api/add-building", err);
            res.json({success: false, message: "Building not advertised"})
        } else {
            res.json({success: true, message: 'Building successfully advertised'})
        }
    })

});


app.use('/api', apiRoutes);


// Start the server
app.listen(port);
console.log('The server is up and ready on: http://localhost:' + port);

